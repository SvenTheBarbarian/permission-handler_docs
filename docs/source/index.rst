.. Test Request documentation master file, created by
   sphinx-quickstart on Sun Dec  1 18:10:09 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Test Request's documentation!
========================================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    intro
    test

Indices and tables
==================

* :ref:`search`
* :ref:`genindex`
