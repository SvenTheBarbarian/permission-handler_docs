Introduction to Test Request Service
====================================

Used to test serverless flows by inserting a request to a service and awaiting a response from the final service.
